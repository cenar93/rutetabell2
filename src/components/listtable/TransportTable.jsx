import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import "moment/locale/nb";
import React from "react";
import uuid from "uuid";
import { getTimeFromNow } from "../TimeUtils";

const useStyles = makeStyles((theme)=>({
  table: {
    width: "20em"
  },
  tableHead: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  container: {
    maxHeight: 1080,
    overflowX: "auto",
    display: "flex",
    justifyContent: "center"
  }
}));

export default function TransportTable({ transportData }) {
  const classes = useStyles();
  return (
    <TableContainer component={Paper} className={classes.container}>
      <Table
        key={transportData.id}
        stickyHeader
        className={classes.table}
        aria-label="customized sticky dense table"
      >
        <TransportTableHead className={classes.tableHead} />
        <TransportTableBody transportDataRows={transportData.rows} />
      </Table>
    </TableContainer>
  );
}

const TransportTableHead = ({className}) => {
  return (
    <TableHead >
      <TableRow >
        <TableCell className={className}>Avgang</TableCell>
        <TableCell className={className} align="left">Linje</TableCell>
      </TableRow>
    </TableHead>
  );
};

const TransportTableBody = ({ transportDataRows}) => {
  return (
    <TableBody key={transportDataRows[0].code+"body"}>
      {transportDataRows.map(transportTime => (
        <TransportTableRow key={uuid()} transportTime={transportTime} />
      ))}
    </TableBody>
  );
};

const TransportTableRow = ({ transportTime }) => {
  return (
    <TableRow key={transportTime.code}>
      <TableCell component="th" scope="row">
        {getTimeFromNow(transportTime.expected)}
      </TableCell>
      <TableCell align="left">{('0' + transportTime.code).slice(-2) + " " + transportTime.frontText}</TableCell>
    </TableRow>
  );
};