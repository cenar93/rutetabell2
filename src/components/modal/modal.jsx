
import React, { useState } from 'react'
import { useSelector, useEffect } from 'react-redux'
import Dialog from '@material-ui/core/Dialog';
import { useDispatch } from 'react-redux'
import { EnturAPI } from '../../Api/Entur';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

import Button from '@material-ui/core/Button';
import useEnturAutocomplete from '../hooks/useEnturAutocomplete';
import "./modal.css";

const useStyles = makeStyles(theme => ({
    button: {
        margin: theme.spacing(1),
    },
    input: {
        display: 'none',
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
    },
}));

export function EnturSearchModal({ setOpenModal, setValue }) {
    const classes = useStyles();
    const [searchTerm, setSearchTerm] = useState("")
    const features = useEnturAutocomplete(searchTerm)
    const closeModal = () => setOpenModal(false);
    const dispatch = useDispatch();
    return (
        <Dialog onClose={()=>dispatch({type: "CLOSE_MODAL"})} open={true}>
            <TextField
                id="outlined-search"
                label="Search field"
                type="search"
                className={classes.textField}
                margin="normal"
                variant="outlined"
                onChange={(event) => setSearchTerm(event.target.value)}
            />
            <div>
                {features.map(data =>
                    (<div 
                        onClick={() => dispatch({type: "CLOSE_MODAL", id: data.properties.id, name: data.properties.label})}
                        className={"entur_modal_item"}>{data.properties.label}</div>)
                )}
            </div>
            <div>
                <Button  variant="contained" color="primary" className={classes.button}>
                    Ok
                </Button>
                <Button onClick={closeModal} variant="contained" className={classes.button}>
                    Avbryt
                </Button>
            </div>
        </Dialog>
    )

}