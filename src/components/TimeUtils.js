import moment from "moment";
import "moment/locale/nb";

export const getTimeFromNow = time => {
    const timeMoment = moment(time).locale("nb");
    const timeNow = moment.now();
    const diff = timeMoment.diff(timeNow, "minutes");
    if (diff > 9) {
        return timeMoment.format("HH:mm");
    }
    return diff + " min";
};