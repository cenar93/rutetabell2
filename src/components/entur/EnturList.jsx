import { useQuery } from "@apollo/react-hooks";
import React from "react";
import { ENTUR_STOP_PLACE_QUERY } from "../../Api/Entur";
import { mapEnturStopPlaceResponseToTable } from "../../Api/enturResponseMapper";
import SimpleExpansionPanel from "../expansionpanel/SimpleExpansionPanel";
import TransportTable from "../listtable/TransportTable";
import { getTimeFromNow } from "../TimeUtils";
import "./EnturList.css";
export default function EnturList({ id }) {
  const { loading, error, data} = useQuery(ENTUR_STOP_PLACE_QUERY, {
    variables: { id: id },
    skip: false
  });

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :( {error}</p>;

  const tableData = mapEnturStopPlaceResponseToTable(data);
  return (
    <div className={"routeTable"}>
      <SimpleExpansionPanel summary={convertTableDataToSummary(tableData)}>
        <TransportTable transportData={tableData} />
      </SimpleExpansionPanel>
    </div>
  );
}

const convertTableDataToSummary = tableData => {
  return (
    tableData.name +
    "  " +
    getTimeFromNow(tableData.rows[0].expected) +
    " - " +
    ("0" + tableData.rows[0].code).slice(-2) +
    " " +
    tableData.rows[0].frontText
  );
};
