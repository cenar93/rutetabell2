import React from "react";
import "./App.css";
import TransportTimesPage from "./pages/entur/TransportTimesPage";

function App() {
  return (
    <div className="App">
      <TransportTimesPage />
    </div>
  );
}

export default App;
