const axios = require('axios').default;

export const Request = {
    get: (url)=>axios.get(`${url}`),
    post: (url, data, headers)=>axios.post(`${url}`)
}
