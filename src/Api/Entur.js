import { Request } from './Requests';
import { gql, ApolloClient, ApolloLink, HttpLink, InMemoryCache } from 'apollo-boost';
import { onError } from 'apollo-link-error';
import { constants } from 'crypto';
import { setContext } from 'apollo-link-context';

const enturGraphqlUri = "https://api.entur.io/journey-planner/v2/graphql";

export const EnturAPI = {
    autocomplete: (text, size) => Request.get(`https://api.entur.io/geocoder/v1/autocomplete?text=${text}&lang=no&size=${size}`)
        .then(res => {
            console.log(res.data)
            return res.data
        })
}

const headerLink = setContext((_, { headers }) => {
    // get the authentication token from local storage if it exists
    const token = localStorage.getItem('token');
    // return the headers to the context so httpLink can read them
    return {
        headers: {
            ...headers,
            "ET-Client-Name": "private-localUtvikling"
        }
    }
});

export const enturGraphqlClient = new ApolloClient({
    link: headerLink.concat(ApolloLink.from([
        onError(({ graphQLErrors, networkError }) => {
            if (graphQLErrors)
                graphQLErrors.forEach(({ message, locations, path }) =>
                    console.log(
                        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
                    ),
                );
            if (networkError) console.log(`[Network error]: ${networkError}`);
        }),
        new HttpLink({
            uri: enturGraphqlUri,
        })
    ])),
    cache: new InMemoryCache()
});

export const ENTUR_STOP_PLACE_QUERY = gql `
query StopPlace($id: String!){
  stopPlace(id: $id) {
    id
    name
    estimatedCalls(timeRange: 72100, numberOfDepartures: 10) {     
      realtime
      aimedArrivalTime
      aimedDepartureTime
      expectedArrivalTime
      expectedDepartureTime
      actualArrivalTime
      actualDepartureTime
      date,
      forBoarding
      forAlighting
      destinationDisplay {
        frontText
      }
      quay {
        id
      }
      serviceJourney {
        journeyPattern {
          line {
            id
            publicCode
            name
            transportMode
          }
        }
      }
    }
  }
}
`
export const ENTUR_QUERY = gql `
query TripInfo($from: String, $to: String) {
    trip(
      from: {
        place: $from
        name: "Vårveien, Drammen"
      }
      to: {
        place: $to
        name:"Nydalen, Oslo"
      }
      numTripPatterns: 3
      minimumTransferTime: 180
      walkSpeed: 1.3
      wheelchair: false
      arriveBy: false
    )
  
  #### Requested fields
    {
      tripPatterns {
        startTime
        duration
        walkDistance
        legs {
          mode
          distance
          realtime
          expectedStartTime
          expectedEndTime
          line {
            id
            publicCode
          }
        }
      }
    }
  }
`;