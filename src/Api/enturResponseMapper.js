export const mapEnturStopPlaceResponseToTable = (response) => {
    const data = response.stopPlace
    return {
        name: data.name,
        id: data.id,
        rows: data.estimatedCalls.map(call => ({
            arrival: call.aimedArrivalTime,
            expected: call.expectedArrivalTime,
            frontText: call.destinationDisplay.frontText,
            code: call.serviceJourney.journeyPattern.line.publicCode,
            name: call.serviceJourney.journeyPattern.line.name,
            transportMode: call.serviceJourney.journeyPattern.line.transportMode
        }))
    }
}