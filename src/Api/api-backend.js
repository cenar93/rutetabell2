import { Request } from './Requests';

export const API = {
    test: () => Request.get("hello").then((response) => response.data),
    stations: () => Request.get("api/stations").then((response) => response.data)
}