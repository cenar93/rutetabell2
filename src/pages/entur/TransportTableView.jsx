import React, { useState } from "react";
import EnturList from "../../components/entur/EnturList";
import "./TransportTableView.css";

const TransportTableView = () => {
  const [stations, setStations] = useState([
    { stasjonId: "NSR:StopPlace:17288" },
    { stasjonId: "NSR:StopPlace:17289" },
    { stasjonId: "NSR:StopPlace:16812" },
    { stasjonId: "NSR:StopPlace:16804" }
  ]);

  if (stations.length === 0) {
    return <div>Loading....</div>;
  }

  return (
    <div className={"TransportTableView"}>
      <div className={"TransportTableViewContent"}>
        {stations.map(station => {
          return (
              <EnturList key={station} id={station.stasjonId} />
          );
        })}
      </div>
    </div>
  );
};

export default TransportTableView;
