import { ApolloProvider } from "@apollo/react-hooks";
import React from "react";
import { enturGraphqlClient } from "../../Api/Entur";
import TransportTableView from "./TransportTableView";

export const TransportTableContext = React.createContext({
  toId: "NSR:StopPlace:16812",
  fromId: "NSR:StopPlace:17289"
});

const TransportTimesPage = () => {
  return (
    <ApolloProvider client={enturGraphqlClient}>
      <TransportTableView />
    </ApolloProvider>
  );
};

export default TransportTimesPage;
